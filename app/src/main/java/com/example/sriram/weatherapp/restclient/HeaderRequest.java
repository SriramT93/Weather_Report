package com.example.sriram.weatherapp.restclient;

import com.example.sriram.weatherapp.activity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class HeaderRequest {
    private HeaderRequest() {
        throw new IllegalStateException("Error..!");
    }

    public static JSONObject getHeaderRequest(final boolean isAuthUrl, final JSONObject headers) {
        try {
            if (headers != null) {
                if (isAuthUrl) {
                    headers.put(BaseActivity.Keys.BASE_URL_KEY, BaseActivity.Urls.BASE_URL_STAGING);
                } else {
                    headers.put(BaseActivity.Keys.BASE_URL_KEY, BaseActivity.Urls.BASE_URL_PRODUCTION);
                }
            } else {
                return null;
            }
        } catch (JSONException e) {
            BaseActivity.logd(e.getMessage());
        }
        return headers;
    }

}
