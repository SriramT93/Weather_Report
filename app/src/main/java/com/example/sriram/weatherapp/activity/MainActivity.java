package com.example.sriram.weatherapp.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sriram.weatherapp.R;
import com.example.sriram.weatherapp.Util.ServiceConstants;
import com.example.sriram.weatherapp.adapter.WeatherAdapter;
import com.example.sriram.weatherapp.helper.GetWeatherInformation;
import com.example.sriram.weatherapp.model.WeatherJsonResponse;
import com.example.sriram.weatherapp.model.WeatherList;
import com.example.sriram.weatherapp.restclient.Listener;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements Listener {
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = MainActivity.this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Weather App");

        FloatingActionButton floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setInstance();
            }
        });


        setInstance();
    }


    private void setInstance() {
        GetWeatherInformation.getWeatherInfo(ServiceConstants.APP_ID, ServiceConstants.CHENNAI_ID, mContext, this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccess(Object successObject) {
        WeatherJsonResponse weatherJsonResponse = (WeatherJsonResponse) successObject;
        if (weatherJsonResponse != null) {
            ArrayList<WeatherList> listArrayListData = new ArrayList<>(weatherJsonResponse.getList());
            if (listArrayListData.size() > 0) {
                String mCity = weatherJsonResponse.getCity().getName();

                ImageView mImageView = findViewById(R.id.imageView);
                TextView mDegree = findViewById(R.id.temp_degree);
                TextView mLocation = findViewById(R.id.temp_location);
                TextView mMode = findViewById(R.id.temp_mode);

                mLocation.setText(mCity);
                mImageView.setImageResource(R.drawable.chennai);
                mDegree.setText(String.valueOf(listArrayListData.get(0).getTemp().getDay())
                        + ServiceConstants.DEGREE);
                mMode.setText(listArrayListData.get(0).getWeather().get(0).getMain());
                RecyclerView recyclerView = findViewById(R.id.recyclerView);
                recyclerView.setHasFixedSize(true);
                recyclerView.setNestedScrollingEnabled(false);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
                recyclerView.setAdapter(new WeatherAdapter(mContext,listArrayListData.subList(1,listArrayListData.size())));
            } else {
                shortToast(mContext, ServiceConstants.NO_DATA);
            }

        } else {
            shortToast(mContext, ServiceConstants.SERVER_ERROR);

        }
    }

    @Override
    public void onFailure(Object failureObject) {
        shortToast(mContext, ServiceConstants.SERVER_ERROR);
    }

    @Override
    public void onError() {
        shortToast(mContext, ServiceConstants.INETRNET_CHECK);
    }
}




