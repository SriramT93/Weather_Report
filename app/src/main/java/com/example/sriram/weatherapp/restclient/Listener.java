package com.example.sriram.weatherapp.restclient;

public interface Listener {
    void onSuccess(Object successObject);

    void onFailure(Object failureObject);

    void onError();

}
