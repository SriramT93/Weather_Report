
package com.example.sriram.weatherapp.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class WeatherList implements Serializable {

    @SerializedName("clouds")
    private Long mClouds;
    @SerializedName("deg")
    private Long mDeg;
    @SerializedName("dt")
    private Long mDt;
    @SerializedName("humidity")
    private Long mHumidity;
    @SerializedName("pressure")
    private Double mPressure;
    @SerializedName("rain")
    private Double mRain;
    @SerializedName("speed")
    private Double mSpeed;
    @SerializedName("temp")
    private Temp mTemp;
    @SerializedName("weather")
    private java.util.List<Weather> mWeather;

    public Long getClouds() {
        return mClouds;
    }

    public Long getDeg() {
        return mDeg;
    }

    public Long getDt() {
        return mDt;
    }

    public Long getHumidity() {
        return mHumidity;
    }

    public Double getPressure() {
        return mPressure;
    }

    public Double getRain() {
        return mRain;
    }

    public Double getSpeed() {
        return mSpeed;
    }

    public Temp getTemp() {
        return mTemp;
    }

    public java.util.List<Weather> getWeather() {
        return mWeather;
    }


}
