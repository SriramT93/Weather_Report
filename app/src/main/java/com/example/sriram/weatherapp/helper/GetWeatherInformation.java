package com.example.sriram.weatherapp.helper;

import android.content.Context;

import com.example.sriram.weatherapp.Util.ServiceConstants;
import com.example.sriram.weatherapp.model.WeatherJsonResponse;
import com.example.sriram.weatherapp.restclient.HeaderRequest;
import com.example.sriram.weatherapp.restclient.Listener;
import com.example.sriram.weatherapp.restclient.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetWeatherInformation {

    private GetWeatherInformation() {
        throw new IllegalStateException(ServiceConstants.SERVER_ERROR);
    }

    public static void getWeatherInfo(String mAppId,String mId, Context context, final Listener listener) {

        if (ServiceConstants.isNetworkAvailable(context)) {
            JSONObject jsonObject = HeaderRequest.getHeaderRequest(true, ServiceConstants.getJsonHeaders());
            RetrofitClient.getInstance(jsonObject).getWeatherList(mAppId,mId).enqueue(new Callback<WeatherJsonResponse>() {
                @Override
                public void onResponse(Call<WeatherJsonResponse> call, Response<WeatherJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<WeatherJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });

        } else {
            listener.onError();
        }

    }
}
