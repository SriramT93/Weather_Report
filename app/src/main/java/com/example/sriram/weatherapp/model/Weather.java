
package com.example.sriram.weatherapp.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Weather implements Serializable {

    @SerializedName("description")
    private String mDescription;
    @SerializedName("icon")
    private String mIcon;
    @SerializedName("id")
    private Long mId;
    @SerializedName("main")
    private String mMain;

    public String getDescription() {
        return mDescription;
    }

    public String getIcon() {
        return mIcon;
    }

    public Long getId() {
        return mId;
    }

    public String getMain() {
        return mMain;
    }


}
