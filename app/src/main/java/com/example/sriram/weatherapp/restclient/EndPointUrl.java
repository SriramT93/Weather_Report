package com.example.sriram.weatherapp.restclient;

import com.example.sriram.weatherapp.Util.ServiceConstants;
import com.example.sriram.weatherapp.activity.BaseActivity;
import com.example.sriram.weatherapp.model.WeatherJsonResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EndPointUrl {
    @GET(BaseActivity.Urls.PASSING_ID)
    Call<WeatherJsonResponse> getList(@Query(ServiceConstants.APP_ID_REQ) String appId,
                                      @Query(ServiceConstants.ID) String ida);
}
