package com.example.sriram.weatherapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sriram.weatherapp.R;
import com.example.sriram.weatherapp.Util.ServiceConstants;
import com.example.sriram.weatherapp.model.WeatherList;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.Weatherholder> {

    private Context mContext;
    private List<WeatherList> dataList;

    public WeatherAdapter(Context context, List<WeatherList> listListData) {
        this.mContext = context;
        this.dataList = listListData;

    }

    @NonNull
    @Override
    public WeatherAdapter.Weatherholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view, parent, false);
        return new Weatherholder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull WeatherAdapter.Weatherholder holder, int position) {
        long unixSeconds = dataList.get(position).getDt();

        Date date = new java.util.Date(unixSeconds * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);
        holder.mDays.setText(formattedDate);
        holder.mDesciption.setText(dataList.get(position).getWeather().get(0).getDescription());
        String mTempMin = String.valueOf(dataList.get(position).getTemp().getMin());
        String mTempMax = String.valueOf(dataList.get(position).getTemp().getMax());
        holder.mDegree.setText(mTempMax + ServiceConstants.BACKSLASH + mTempMin + ServiceConstants.DEGREE);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class Weatherholder extends RecyclerView.ViewHolder {
        private TextView mDays;
        private TextView mDesciption;
        private TextView mDegree;

        Weatherholder(View itemView) {
            super(itemView);

            mDays = itemView.findViewById(R.id.text_days);
            mDesciption = itemView.findViewById(R.id.text_description);
            mDegree = itemView.findViewById(R.id.text_temp);
        }
    }
}
