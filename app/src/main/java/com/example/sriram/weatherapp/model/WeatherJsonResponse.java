
package com.example.sriram.weatherapp.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WeatherJsonResponse implements Serializable {

    @SerializedName("city")
    private City mCity;
    @SerializedName("cnt")
    private Long mCnt;
    @SerializedName("cod")
    private String mCod;
    @SerializedName("list")
    private java.util.List<WeatherList> mWeatherList;
    @SerializedName("message")
    private Double mMessage;

    public City getCity() {
        return mCity;
    }

    public Long getCnt() {
        return mCnt;
    }

    public String getCod() {
        return mCod;
    }

    public java.util.List<WeatherList> getList() {
        return mWeatherList;
    }

    public Double getMessage() {
        return mMessage;
    }

    public static class Builder {

        private City mCity;
        private Long mCnt;
        private String mCod;
        private java.util.List<WeatherList> mWeatherList;
        private Double mMessage;

        public WeatherJsonResponse.Builder withCity(City city) {
            mCity = city;
            return this;
        }

        public WeatherJsonResponse.Builder withCnt(Long cnt) {
            mCnt = cnt;
            return this;
        }

        public WeatherJsonResponse.Builder withCod(String cod) {
            mCod = cod;
            return this;
        }

        public WeatherJsonResponse.Builder withList(java.util.List<WeatherList> weatherList) {
            mWeatherList = weatherList;
            return this;
        }

        public WeatherJsonResponse.Builder withMessage(Double message) {
            mMessage = message;
            return this;
        }

        public WeatherJsonResponse build() {
            WeatherJsonResponse weatherJsonResponse = new WeatherJsonResponse();
            weatherJsonResponse.mCity = mCity;
            weatherJsonResponse.mCnt = mCnt;
            weatherJsonResponse.mCod = mCod;
            weatherJsonResponse.mWeatherList = mWeatherList;
            weatherJsonResponse.mMessage = mMessage;
            return weatherJsonResponse;
        }

    }

}
