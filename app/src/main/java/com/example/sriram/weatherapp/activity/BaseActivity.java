package com.example.sriram.weatherapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public class BaseActivity extends AppCompatActivity {
    public enum LogLevel {OFF, DEBUG, VERBOSE}

    private static LogLevel sLoggingLevel = LogLevel.OFF;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


    }

    public static void logd(String message, Object... params) {
        if (logDebugEnabled()) {
            Log.d(message, String.format(message, params));
        }
    }

    public static boolean logDebugEnabled() {
        return sLoggingLevel.ordinal() >= LogLevel.DEBUG.ordinal();
    }


    public static void routeToActivityStackBundle(final Context activityContext, final Class<? extends Activity> destinationActivity, Bundle bundle) {
        activityContext.startActivity(new Intent(activityContext, destinationActivity).putExtras(bundle));
    }

    public static void routeToActivityStack(final Context activityContext, final Class<? extends Activity> destinationActivity) {
        activityContext.startActivity(new Intent(activityContext, destinationActivity));
    }


    public class Keys {
        private Keys() {
            throw new IllegalStateException("Service Constants");
        }

        public static final String BASE_URL_KEY = "base_url";
    }

    public class Urls {

        private Urls() {
            throw new IllegalStateException("Service Constants");
        }

        public static final String BASE_URL_STAGING = "https://openweathermap.org/data/2.5/forecast/";
        public static final String BASE_URL_PRODUCTION = "https://openweathermap.org/data/2.5/forecast/";

        public static final String PASSING_ID = "daily?";

    }


    public static void shortToast(Context con, String msg) {
        Toast toast = Toast.makeText(con, msg, Toast.LENGTH_SHORT);
        toast.show();
    }



}
