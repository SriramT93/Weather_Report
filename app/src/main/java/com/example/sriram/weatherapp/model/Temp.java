
package com.example.sriram.weatherapp.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Temp implements Serializable {

    @SerializedName("day")
    private Double mDay;
    @SerializedName("eve")
    private Double mEve;
    @SerializedName("max")
    private Double mMax;
    @SerializedName("min")
    private Double mMin;
    @SerializedName("morn")
    private Double mMorn;
    @SerializedName("night")
    private Double mNight;

    public Double getDay() {
        return mDay;
    }

    public Double getEve() {
        return mEve;
    }

    public Double getMax() {
        return mMax;
    }

    public Double getMin() {
        return mMin;
    }

    public Double getMorn() {
        return mMorn;
    }

    public Double getNight() {
        return mNight;
    }


}
