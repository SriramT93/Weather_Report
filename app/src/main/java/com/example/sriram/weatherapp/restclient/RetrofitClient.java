package com.example.sriram.weatherapp.restclient;

import com.example.sriram.weatherapp.activity.BaseActivity;
import com.example.sriram.weatherapp.model.WeatherJsonResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static JSONObject mHeaderObject = null;
    private EndPointUrl apiService;
    private String baseUrl = "";


    private RetrofitClient() {
        initService();
    }

    public static RetrofitClient getInstance(JSONObject headerObject) {
        mHeaderObject = headerObject;
        return new RetrofitClient();
    }

    private void initService() {
        Retrofit retrofit = createAdapter().build();
        apiService = retrofit.create(EndPointUrl.class);
    }




    private Retrofit.Builder createAdapter() {

        try {
            baseUrl = mHeaderObject.getString(BaseActivity.Keys.BASE_URL_KEY);
        } catch (JSONException e) {
            BaseActivity.logd(e.getMessage());
        }

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(addCustomHeaders(mHeaderObject))
                .addConverterFactory(GsonConverterFactory.create());
    }

    private OkHttpClient addCustomHeaders(final JSONObject authorizationValue) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.connectTimeout(30, TimeUnit.SECONDS);
        httpClient.writeTimeout(30, TimeUnit.SECONDS);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                if (authorizationValue != null) {
                    final Request.Builder request = chain.request().newBuilder();

                    Iterator<String> iterator = authorizationValue.keys();
                    while (iterator.hasNext()) {
                        String key = iterator.next();
                        try {
                            if (!key.contains(BaseActivity.Keys.BASE_URL_KEY)) {
                                Object value = authorizationValue.get(key);
                                request.addHeader(key, value.toString());
                            }
                        } catch (JSONException e) {
                            BaseActivity.logd(e.getMessage());
                        }
                    }

                    return chain.proceed(request.build());
                }

                return null;
            }
        });

        mHeaderObject = null;

        return httpClient.build();
    }


    public Call<WeatherJsonResponse> getWeatherList(String appId, String mId) {
        return apiService.getList(appId,mId);
    }
}
