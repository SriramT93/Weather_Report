package com.example.sriram.weatherapp.Util;

import android.content.Context;
import android.net.ConnectivityManager;

import com.example.sriram.weatherapp.activity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceConstants {

    public static final String APP_ID = "b6907d289e10d714a6e88b30761fae22";
    public static final String APP_ID_REQ = "appid";
    public static final String ID = "id";
    public static final String CHENNAI_ID = "1264527";
    public static final String BANGALORE_ID = "1277333";
    public static final String MUMBAI_ID = "1275339";
    public static final String NEW_DELHI_ID = "1261481";

    public static final String INETRNET_CHECK = "Please check your Internet Connection";
    public static final String SERVER_ERROR = "There was a problem connecting to the server. Please try again.";

    /*JSON Formation*/
    private static final String HEADER_TYPE = "Content-Type";
    private static final String HEADER_VALUE = "application/json";
    public static final String NO_DATA = "No data found..!";
    public static final String BACKSLASH = " / ";
    public static final String DEGREE = (char) 0x00B0 + " C ";


    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected();
    }

    /*Headers Formation*/
    public static JSONObject getJsonHeaders() {
        JSONObject headers = new JSONObject();
        try {
            headers.put(HEADER_TYPE, HEADER_VALUE);
        } catch (JSONException e) {
            BaseActivity.logd(e.getMessage());
        }
        return headers;
    }

}
